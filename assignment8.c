#include<stdio.h>

struct student{

  char name[20];
  char subject[20];
  float marks;
};

int main()
{
    int i, n;
    struct student st[10];

    printf("How many student's details do you want to record: ");
    scanf("%d", &n);


    for(i=0; i<n; i++)
    {
        printf("\nEnter details of student %d\n", i+1);
        scanf("%s", &st[i].name);
        scanf("%s", &st[i].subject);
        scanf("%f", &st[i].marks);

    }
   printf("\nList of student details\n\n");

   for(i=0; i<n; i++)
   {
       printf("Name: %s\n", st[i].name);
       printf("Subject: %s\n", st[i].subject);
       printf("Marks: %.1f\n", st[i].marks);
       printf("\n");
   }

   return 0;
}
